"use strict";

let round = 1;

class Sprite{

    oddEven() {
        if (round % 2 == 0) {
            var active = 2;
            var activePlayer = player2;
            var activePlayerString = "player2";
            var inactivePlayer = player1;
            var inactivePlayerString = "player1";
            var activePlayerId = "#player2Img";
            var activeInventory = "inventory2";
            document.getElementById("displayPlayer").innerHTML = "player2";

            //Réinisialisation defense
            activePlayer.defense = 0;

        } else {
            var active = 1;
            var activePlayer = player1;
            var activePlayerString = "player1";
            var inactivePlayer = player2;
            var inactivePlayerString = "player2";
            var activePlayerId = "#player1Img";
            var activeInventory = "inventory1";
            document.getElementById("displayPlayer").innerHTML = "player1";

            //Réinisialisation defense
            activePlayer.defense = 0;
        }

        return[activePlayer, activePlayerString, activePlayerId, inactivePlayer, activeInventory, inactivePlayerString];
    }

    //gestion des colisions
    collision(){

        let oddEvenArray = this.oddEven();

        //Bord de la map
        const sideRight = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
        const sideLeft = [1, 11, 21, 31, 41, 51, 61, 71, 81, 91];
        //Positions des obstacles
        const rock = document.getElementsByClassName('1');
        let arrayRock = [];

        for(let i=0;i<rock.length;i++){
            let convert = parseInt(rock[i].id);
            arrayRock.push(convert);
        }

        let moveP = oddEvenArray[0].position;

        switch(event.which){
            case 37:
                moveP -= 1;
                var searchRock = jQuery.inArray(moveP, arrayRock);
                var searchLeft = jQuery.inArray(oddEvenArray[0], sideLeft);
                break;
            case 38:
                moveP += 10;
                var searchRock = jQuery.inArray(moveP, arrayRock);
                break;
            case 39:
                moveP += 1;
                var searchRock = jQuery.inArray(moveP, arrayRock);
                var searchRight = jQuery.inArray(oddEvenArray[0].position, sideRight);
                break;
            case 40:
                moveP -= 10;
                var searchRock = jQuery.inArray(moveP, arrayRock);
                break;
        }

        if(searchRock !== -1 || moveP > 100 || moveP < 1 || oddEvenArray[0].move === 0){
            document.getElementById("notif").innerHTML = "Déplacement impossible";
        }else{
            if(event.which === 37 && searchLeft !== -1){
                document.getElementById("notif").innerHTML = "Déplacement impossible";
            }else if(event.which === 39 && searchRight !== -1){
                document.getElementById("notif").innerHTML = "Déplacement impossible";
            }else{
                oddEvenArray[0].position = moveP;
                oddEvenArray[0].move -= 1;
                document.getElementById(oddEvenArray[1]+"Move").innerHTML = oddEvenArray[0].move;
                move();
            }
        }
        function move(){
            $(oddEvenArray[2]).remove();
            $("#"+ oddEvenArray[0].position).append(oddEvenArray[0].skin);
            spriteObj.detectWeapon();
            spriteObj.detectFight();
        }
    }

    detectWeapon(){

        let oddEvenArray = this.oddEven();

        const weapon = document.getElementsByClassName('weapon');
        let arrayWeapon = [];

        for(let i=0;i<weapon.length;i++){
            let parentWp = $(weapon[i]).parent().attr("id");
            let convert = parseInt(parentWp);
            arrayWeapon.push(convert);
        }

        let searchWp = jQuery.inArray(oddEvenArray[0].position, arrayWeapon);

        if(searchWp !== -1){
            let weaponSelect = weapon[searchWp];
            spriteObj.getWeapon(weaponSelect);
        }
    }

    detectFight(){

        let oddEvenArray = this.oddEven();

        const arrayPosFight = [];
        //Positions qui déclenchent le fight
        const fightLeft = +oddEvenArray[3].position - 1;
        const fightRight = +oddEvenArray[3].position + 1;
        const fightTop = +oddEvenArray[3].position + 10;
        const fightBot = +oddEvenArray[3].position - 10;

        arrayPosFight.push(fightLeft, fightRight, fightTop, fightBot);
        let searchFight = jQuery.inArray(oddEvenArray[0].position, arrayPosFight);
        if(searchFight !== -1){
            $('#attackBtn').show();
        }else{
            $('#attackBtn').hide();
        }
    }

    getWeapon(weaponSelect){

        let oddEvenArray = this.oddEven();

        switch(weaponSelect.id){
            case "imgwp1":
                oddEvenArray[0].damage = weapon1.damage;
                document.getElementById(oddEvenArray[1]+"Wp").innerHTML = weapon1.name;
                break;
            case "imgwp2":
                oddEvenArray[0].damage = weapon2.damage;
                document.getElementById(oddEvenArray[1]+"Wp").innerHTML = weapon2.name;
                break;
            case "imgwp3":
                oddEvenArray[0].damage = weapon3.damage;
                document.getElementById(oddEvenArray[1]+"Wp").innerHTML = weapon3.name;
                break;
            case "imgwp4":
                oddEvenArray[0].damage = weapon4.damage;
                document.getElementById(oddEvenArray[1]+"Wp").innerHTML = weapon4.name;
                break;
        }

        //Si un weapon est déja dans l'inventaire = le déposer
        if(oddEvenArray[0].inventory !== 0){
            $("#"+oddEvenArray[0].position).append(oddEvenArray[0].inventory);
        }

        document.getElementById(oddEvenArray[1]+"Dmg").innerHTML = oddEvenArray[0].damage;
        //supp weapon de la map
        $("#"+weaponSelect.id).remove();
        //add inventaire
        $("#"+oddEvenArray[4]).append(weaponSelect);
        oddEvenArray[0].inventory = weaponSelect;
    }

}

const spriteObj = new Sprite();