"use strict";
class Player{

    //constructeur player
    constructor(name, position, HP, move, weapon, damage, skin, inventory, defense) {
        this.name = name;
        this.position = position;
        this.HP = 100;
        this.move = 3;
        this.weapon = weapon;
        this.damage = 10;
        this.skin = skin;
        this.inventory = inventory;
        this.defense = defense;
    }
}