"use strict";

class Fight{

    attack(){

        let oddEvenArray = spriteObj.oddEven();

        if(oddEvenArray[0].move <= 0){
            document.getElementById("notif").innerHTML = "Pas d'attaque disponible";
        }else{
            if(oddEvenArray[3].defense === 1){
                let hit = oddEvenArray[0].damage / 2;
                oddEvenArray[3].HP -= hit;
            }else{
                let hit = oddEvenArray[0].damage;
                oddEvenArray[3].HP -= hit;
            }


            oddEvenArray[0].move = 0;
            document.getElementById(oddEvenArray[1]+"Move").innerHTML = oddEvenArray[0].move;
            $('#attackBtn').hide();

            document.getElementById(oddEvenArray[5]+"Hp").innerHTML = oddEvenArray[3].HP;
        }
    }

    victory(){

        let oddEvenArray = spriteObj.oddEven();

        if(oddEvenArray[3].HP <= 0){
            $("body").empty();
            $("body").append("<h2 class='victoryTxt'>VICTORY "+oddEvenArray[1]+" !!!</h2>");
            $("body").append("<btn id='replay' onclick = 'fightObj.again();'>Play again</btn>");
            uiObj.styleEnd();
        }
    }

    again(){
        //reload btn victory
        window.location.reload();
    }


    defense(){

        let oddEvenArray = spriteObj.oddEven();

        if(oddEvenArray[0].move <= 0){
            document.getElementById("notif").innerHTML = "Pas de défense disponible";
        }else{
            oddEvenArray[0].defense = 1;
            oddEvenArray[0].move = 0;
            document.getElementById(oddEvenArray[1]+"Move").innerHTML = oddEvenArray[0].move;
            document.getElementById("notif").innerHTML = "Défense active";
        }
    }


}
const fightObj = new Fight();
