"use strict";

//Objets player
const player1 = new Player("Player 1", 0, 100, 3, 0, 10, "<img id='player1Img' src='assets/player1.png' style='position: absolute;margin-top: -20px;margin-left: -20px;'>", 0, 0);
const player2 = new Player("Player 2", 0, 100, 3, 0, 10, "<img id='player2Img' src='assets/player2.png' style='position: absolute;margin-top: -20px;margin-left: -20px;'>", 0, 0);

//Objets weapon
const weapon1 = new Weapon("Sword", 0, 20, "<img class='weapon' id='imgwp1' src='assets/weapon1.png' style='position: absolute;margin-top: -20px;margin-left: -20px;'>");
const weapon2 = new Weapon("Axe", 0, 20, "<img class='weapon' id='imgwp2' src='assets/weapon2.png' style='position: absolute;margin-top: -20px;margin-left: -20px;'>");
const weapon3 = new Weapon("Spear", 0, 25, "<img class='weapon' id='imgwp3' src='assets/weapon3.png' style='position: absolute;margin-top: -20px;margin-left: -20px;'>");
const weapon4 = new Weapon("Scepter", 0, 30, "<img class='weapon' id='imgwp4' src='assets/weapon4.png' style='position: absolute;margin-top: -20px;margin-left: -20px;'>");

//Array contenant les emplacements aléatoires
const arraySpawn = [];

//SPAWN = Génère les emplacements sur la map et attribut les objets à ceux-ci
class Spawn{

    generatePosition(){
        //Array contenant les emplacements libres
        const arraySpawnFree = document.getElementsByClassName('0');

        //Générations des emplacements

        //Emplacements des players
        let pSpawn = arraySpawnFree[Math.floor(Math.random() * 25)];
        arraySpawn.push(pSpawn);
        pSpawn = arraySpawnFree[Math.floor(Math.random() * (89 - 65) + 65)];
        arraySpawn.push(pSpawn);

        for (let i = 0; i < 4; i++) {
            let spawnSelect = arraySpawnFree[Math.floor(Math.random() * arraySpawnFree.length)];
            let check = $.inArray(spawnSelect, arraySpawn);

            while (check !== -1){
                spawnSelect = arraySpawnFree[Math.floor(Math.random() * arraySpawnFree.length)];
                check = $.inArray(spawnSelect, arraySpawn);
            }
            arraySpawn.push(spawnSelect);
        }
    }

    refreshPosition(){
        //actualise .position
        player1.position = +$(arraySpawn[0]).attr("id");
        player2.position = +$(arraySpawn[1]).attr("id");

        weapon1.position = arraySpawn[2];
        weapon2.position = arraySpawn[3];
        weapon3.position = arraySpawn[4];
        weapon4.position = arraySpawn[5];

        let arrayWeaponPosition = [];
    }

    putPlayer(){
        //placement en fonction de .position
        $("#"+ player1.position).append(player1.skin);
        $("#"+ player2.position).append(player2.skin);

        $(weapon1.position).append(weapon1.skin);
        $(weapon2.position).append(weapon2.skin);
        $(weapon3.position).append(weapon3.skin);
        $(weapon4.position).append(weapon4.skin);
    }

    place(){
        this.generatePosition();
        this.refreshPosition();
        this.putPlayer();
    }

}