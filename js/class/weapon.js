"use strict";
class Weapon {

    //constructeur weapon
    constructor(name, position, damage, skin) {
        this.name = name;
        this.position = position;
        this.damage = damage;
        this.skin = skin;
    }
}