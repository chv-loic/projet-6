"use strict";

class UserInterface {

    //affichage info joueurs
    displayInfo(){

        //Player 1
        document.getElementById("player1Hp").innerHTML = player1.HP;
        document.getElementById("player1Dmg").innerHTML = player1.damage;
        document.getElementById("player1Wp").innerHTML = player1.weapon;
        document.getElementById("player1Move").innerHTML = player1.move;

        //Player 2
        document.getElementById("player2Hp").innerHTML = player2.HP;
        document.getElementById("player2Dmg").innerHTML = player2.damage;
        document.getElementById("player2Wp").innerHTML = player2.weapon;
        document.getElementById("player2Move").innerHTML = player2.move;
    }

    next(){
        document.getElementById("notif").innerHTML = "";
        player1.move = 3;
        player2.move = 3;
        document.getElementById("player1Move").innerHTML = player1.move;
        document.getElementById("player2Move").innerHTML = player2.move;

        round += 1;
        document.getElementById("tour").innerHTML = round;

        spriteObj.detectFight();
    }

    styleGenerator() {

        /***************STYLE*****************/

        //ajout du css aux elements

        $(game).css({
            "margin-left": "auto",
            "margin-right": "auto",
            "width": "fit-content"
        });

        //affichage herbe
        $("td").css({
            "height": "65px",
            "width": "65px",
            "background-image": "url(assets/img1.png)",
            "text-align":"center"
        });
        $(".1").css({
            "height": "65px",
            "width": "65px",
            "background-image": "url(assets/img2.png)"
        });
    }

    styleEnd() {
        $("body").css({
            "text-align": "center"
        });

        $(".victoryTxt").css({
            "font-family": "'Press Start 2P', cursive",
            "text-align": "center",
            "margin-top": "50px",
            "margin-bottom": "50px"
        });

        $("#replay").css({
            "cursor": "pointer",
            "background-color": "crimson",
            "color": "#fff",
            "padding": "10px",
            "z-index": "0",
            "font-family": "'Press Start 2P', cursive"
        });
    }
}

const uiObj = new UserInterface();