"use strict";

let arrayMap = [];

class MapGenerator {

        generate(){
            //Génération de la map

            $(colgame).append('<table cellspacing="0" id="game"></table>');

            for (let i = 0; i < 90; i++) {
                arrayMap.push("0");
            }
        }

        add(){
            //ajout de valeurs 1
            for (let i = 0; i < 10; i++) {
                arrayMap.push("1");
            }
        }


        sort(){
            //melange array
            arrayMap.sort(function (a, b) {
                return 0.5 - Math.random()
            });
        }

        createTr(){
            //creation des tr avec id unique
            for (let i = 0; i < 10; i++) {
                let id = i + 1;
                $(game).append('<tr id=row' + id + '></tr>');
            }
        }

        createSpan(){
            //creation des span avec id unique et class (1 ou 0)
            for (let i = 0; i < arrayMap.length; i++) {
                let id = i + 1;
                $(game).append('<td id=' + id + ' class="' + arrayMap[i] + '"> </td>');
            }
        }

        putTd(){
            //mettre les td 10 par 10 dans les tr
            for (let i = 1; i <= 10; i++) {

                let id = "row" + i;

                for (let j = 0; j < 99; j += 10) {

                    let max = 10 + j;

                    var slice = $('td').slice(j, max);
                }

                $("#" + id).append(slice);
            }
        }

        build(){
            this.generate();
            this.add();
            this.sort();
            this.createTr();
            this.createSpan();
            this.putTd();
        }

}