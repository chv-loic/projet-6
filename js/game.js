$(document).ready(function () {

    /****************BASIC FUNCTION*************/

    $('#attackBtn').hide();

    /************INSTANCIATION***************/

    const mapObj = new MapGenerator();
    const spawnObj = new Spawn();

    /*******************CALL*********************/

    mapObj.build();
    spawnObj.place();
    spriteObj.oddEven();
    uiObj.displayInfo();
    uiObj.styleGenerator();
});

$(document).keydown(function keyboard(e) {
    document.getElementById("notif").innerHTML = "";
    spriteObj.collision();
})